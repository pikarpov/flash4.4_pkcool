#!/bin/bash
# Sedov

echo "Sedov_1d!"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/pfs/FLASH/FLASH4.2/object
gmake clean

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd ~/pfs/FLASH/FLASH4.2 
./setup Sedov_1d -debug -auto +spherical -1d


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ -f ~/pfs/FLASH/FLASH4.2/source/Simulation/SimulationMain/Sedov_1d/Config ]
then
cd ~/pfs/FLASH/FLASH4.2/object
gmake
else 
echo NO CONFIG FILE
exit
fi



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ -f ~/pfs/FLASH/FLASH4.2/object/Flash.o ]
then
echo GMAKE SUCCESSFUL
else 
echo GMAKE NOT SUCCESSFUL
exit
fi



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

cp ~/pfs/FLASH/FLASH4.2/object/flash4 ~/pfs/FLASH/FLASH4.2/rundir/run_Sedov_1d/flash4_ncl
#cp ~/pfs/FLASH/FLASH4.2/source/Simulation/SimulationMain/Sedov_1d/flash.par ~/pfs/FLASH/FLASH4.2/rundir/run_Sedov_1d/flash.par
#cp ~/pfs/FLASH/FLASH4.2/source/Simulation/SimulationMain/Sedov_1d/sedov_1d.pbs ~/pfs/FLASH/FLASH4.2/rundir/run_Sedov_1d/sedov_1d.pbs

#cd  ~/pfs/FLASH/FLASH4.2/rundir/run_Sedov/
#qsub sedov.pbs
#qstat

echo Done!
