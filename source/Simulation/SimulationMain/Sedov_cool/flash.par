#	Runtime parameters for the Sedov explosion problem.

#	Parameters for initial model

#		Ambient pressure and density, explosion energy, initial
#		radius of perturbation, and location of explosion center

useCool = .true.

tradmin = 1.E4
tradmax = 1.E8
dradmin = 1.E-27        #min rho PKdef
dradmax = 1.E-19        #max rho PKdef

sim_pAmbient	= 1.38E-10 #1.38E-8
sim_rhoAmbient	= 1.67E-22 #1.67E-20
#smallT	= 1.0E4
#smallt = 1.0E4
smlrho = 1.E-32 #1.E-30
smallp = 1.E-20 #1.E-18
sim_expEnergy	= 1.0E51
sim_rInit		= 1.0E18
sim_xctr		= 0

swept_mass = 50. #factor of the original
masse = 6.E33

#		Gas ratio of specific heats

gamma           = 1.667

#	Computational volume parameters

#		Grid dimensionality and geometry

geometry = "spherical"

#		Size of computational volume

xmin		= 0.
xmax		= 5.0E19 #10pc

#		Boundary conditions


xl_boundary_type = "reflect"
xr_boundary_type = "outflow"

#	Simulation time, I/O  parameters

cfl		= 0.8
basenm          = "sedov_"
restart         = .false.


#	checkpoint file output parameters
checkpointFileIntervalTime = 3.15e10
checkpointFileIntervalStep = 0
checkpointFileNumber = 0

#	plotfile output parameters
plotfileIntervalTime = 3.15e10
plotfileIntervalStep = 0
plotfileNumber = 0

#tinitial	= 1d7
nend        = 10000000
dtinit		= 1d-5
dtmin		= 1d6
dtmax		= 3.15d10
tmax        = 1d13

run_comment     = "Sedov explosion"
log_file        = "sedov.log"
eintSwitch     = 1.e-4

plot_var_1 = "dens"
plot_var_2 = "pres"
plot_var_3 = "temp"
plot_var_4 = "enuc"
plot_var_5 = "eint"
#plot_var_6 = "ener"

#  Adaptvie Grid refinement parameters

lrefine_max     = 8
refine_var_1 = "dens"
refine_var_2 = "pres"
refine_var_3 = "eint"


# Uniform Grid specific parameters 
# see note below for more explanation
iGridSize = 512   #global number of gridpoints along x, excluding gcells
#jGridSize = 8   #global number of gridpoints along y, excluding gcells
#kGridSize = 1
iProcs = 16	#num procs in i direction
#jProcs = 1	#num procs in j direction
#kProcs = 1


# When using UG, iProcs, jProcs and kProcs must be specified.
# These are the processors along each of the dimensions
#FIXEDBLOCKSIZE mode ::
# When using fixed blocksize, iGridSize etc are redundant in
# runtime parameters. These quantities are calculated as 
# iGridSize = NXB*iprocs
# jGridSize = NYB*jprocs
# kGridSize = NZB*kprocs
#NONFIXEDBLOCKSIZE mode ::
# iGridSize etc must be specified. They constitute the global
# number of grid points in the physical domain without taking 
# the guard cell into account. The local blocksize is calculated
# as iGridSize/iprocs  etc.
 
 
## -------------------------------------------------------------##
##  SWITCHES SPECIFIC TO THE UNSPLIT HYDRO SOLVER               ##
#	I. INTERPOLATION SCHEME:
order		= 3      # Interpolation order (first/second/third/fifth order)
slopeLimiter    = "mc"   # Slope limiters (minmod, mc, vanLeer, hybrid, limited)
LimitedSlopeBeta= 1.     # Slope parameter for the "limited" slope by Toro
charLimiting	= .false. # Characteristic limiting vs. Primitive limiting

use_avisc	= .true. # use artificial viscosity (originally for PPM)
cvisc		= 0.1     # coefficient for artificial viscosity
use_flattening	= .false. # use flattening (dissipative) (originally for PPM)
use_steepening	= .false. # use contact steepening (originally for PPM)
use_upwindTVD	= .false. # use upwind biased TVD slope for PPM (need nguard=6)

#	II. RIEMANN SOLVERS:
RiemannSolver	= "HLLC"       # Roe, HLL, HLLC, LLF, Marquina
entropy         = .false.     # Entropy fix for the Roe solver

#	III. STRONG SHOCK HANDELING SCHEME:
shockDetect	= .false.     # Shock Detect for numerical stability
## -------------------------------------------------------------##

## ---------------------------------------------------------------##
##  SWITCHES SPECIFIC TO THE SUPER-TIME-STEPPING (STS) ALGORITHM  ##
##  NOTE: For details on using STS runtime parameters, please     ##
##        refer to user's guide (Driver chapter).                 ##
useSTS                  = .false.
nstepTotalSTS           = 5
nuSTS                   = 0.2
## ---------------------------------------------------------------##
