'''
Opacity calculation for Hw 3, Problem 4

The opacity tables were taken from Opacity Project.
http://opacity-cs.obspm.fr/Browser/Browser.jsp

The data import functions were taken from my plotting scripts.

-Platon Karpov
'''

from __future__ import print_function
from __future__ import division
from scipy.interpolate import griddata
from numpy import genfromtxt
import xlrd
import sys
import numpy as np
import sys

def main():
    filename = r'/home/pkarpov/FLASH4.4/Plotting/cooling_test.xlsx'
    sheetnumb = 0
    
    T = float(sys.argv[1])
    rho = float(sys.argv[2])
    
    points, grid = load(filename, sheetnumb)

    Lambda = interpolate(points, grid, T, rho)
    
    print(Lambda)
    #return Lambda

def load(filename, sheetnumb):
    #import the data from a file into values[]
    values = ImportData(filename,sheetnumb)
    
    #assign 1st row as logRho 'axis' values
    logRho = []
    for i in range(1,len(values)):
        logRho.append(values[i][0])
    
    #assign 1st column as logT 'axis' values
    logT = values[0][1:]  

    #delete the logRho and logT axis from the main data set
    values.pop(0)
    for i in range(len(values)):
        values[i].pop(0)    

    #populate points[] with each data point coordinate pair
    points = []
    for R in logRho:
        for T in logT:
            points.append([R, T])
    
    #reshape values into a 1D array, grid[], and put in the order of points[]
    grid = []    
    for column in values:
        for element in column:
            grid.append(element)

    return points, grid    
 
   
def interpolate(points, grid, T, rho):
    
    #set the powers of R and T to look up in the table
    target_R = np.log10(rho)  
    target_T = np.log10(T)  
    #print(target_R, target_T)
           
    #call scipy.interpolate
    Lambda = griddata(points, grid, (target_R, target_T), method='linear')
    
    return Lambda
    
    
#--------------Data Import Functions----------------------

def ImportData(filename, sheetnumb):
    global content
    global sheet
    values = []

    if filename.endswith('.xlsx') or filename.endswith('.xls'):
        content = xlrd.open_workbook(filename, 'r')

        sheet = content.sheet_by_index(sheetnumb)
        column = sheet.ncols
        row = sheet.nrows
        excel = True

    elif filename.endswith('.csv') or filename.endswith('.txt') or filename.endswith('.dat'):
        print(r'Delimeter:  %r'% delimiter(filename))
        content = genfromtxt(filename, delimiter=delimiter(filename))
        column = len(content[0])
        row = len(content)
        excel = False

    else:
        sys.exit("Wth, mate?")


    #print('# of Columns: %d'% column)
    #print('# of Rows:    %d'% row)

    for j in range(column):
        data = []
        counter = 0
        for i in range(0, row):

            try:
                data.append(float(point(i,j, excel)))
            except:
                if counter == 0:
                    #print('Column %d title:  %s'%(j,point(i,j, excel)))
                    counter +=1
                else:
                    break

        values.append(data)
    return values


def delimiter(filename):
    with open(filename, 'r') as content:
        header=content.readline()
        if header.find(";")!=-1:
            return ";"
        if header.find(",")!=-1:
            return ","
        if header.find("\t")!=-1:
            return "\t"
        if header.find("  ")!=-1:
            return "  "    
        if header.find(" ")!=-1:
            return " "  
    return "Could not detect column delimiter"


def point(i,j, excel):
    if excel:
        return sheet.cell(i,j).value
    else:
        return content[i,j]
    
if __name__=='__main__':
    main()
    
