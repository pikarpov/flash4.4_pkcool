'''
Plotting Potato is here to plot:
    column data from a file in supported formats:
        .xlsx, .xls (data taken from first sheet by default)
        .csv, .txt, .dat (tab, comma, semicolon delimited)
    linear, semilog, log funcitons

-To set filepath see PlotData()
-To set fit function see func()
-To edit various parameters of the graphs,
refer to main() or to a corresponding def:
[PlotData or PlotFunction or FitFunction or FitPoly] 

By default, everything will be constructed on the same plot.

If you will try to change styles and want to revert to default,
make sure to re-run the code in a new console.

gl hf

-Platobob
'''
import matplotlib
matplotlib.use('Agg')
from matplotlib import style
import matplotlib.pyplot as plt
from numpy import genfromtxt
import xlrd
import sys
import numpy as np
import string
import sys
try:
    from Fitting import *
except ImportError:
    pass

def main():
    elm = sys.argv[1]
    plt.figure(figsize=(12,9), dpi=80)

    line = ['-']
    cool = [-1]

    ratio, names, elements = Ratios(elm)    
    comb = []
    for c in cool:
        for i in range(len(names)):
            filename = r'datafile%s.txt'%names[i]  
            n = Count(filename)
            sheetnumb = 0
            values = ImportData(filename, sheetnumb, n)
            x = values[0]     #Column containing x-values
            y = values[c]     #Column containing y-values
            y = [j*ratio[i] for j in y]
            print(elements[i])

            if i == 0:
                comb = y
            else:
                for j in range(len(comb)):
                    comb[j] = comb[j]+y[j]
    
    f = open('cooling_table_%s.txt'%elm, 'w+')
    
    #f.write('Temp[K]\tLambda[erg cm^3 s^-1]\n')
    f.write('%d\n'%len(x))
    for i in range(len(x)):
        f.write('%.2e\t%.2e\n'%(x[i], comb[i]))
    f.close()
    
    plt.figure(figsize=(10,5), dpi=80)
    plt.loglog(x, comb, basex=10, basey=10, marker = 'None', label = 'comb')
    plt.grid(True)
    plt.tick_params(axis='x', labelsize=12)
    plt.tick_params(axis='y', labelsize=12)
    plt.xlabel(r'Temp [K]', fontsize=18)
    plt.ylabel(r'$\Lambda\;/\rho^{2} \;\; [erg \; cm^{3} \; g^{-2} \; s^{-1} ]$', fontsize=18)
    plt.title(r'Z=1', fontsize=22)
    plt.savefig('cooling_table_%s.png'%elm, format='png')
    
    
def Count(filename):
    f = open(filename, 'r')
    n = 0
    for line in f:
        if line.find("1.00e+04")!=-1:
            break
        n+=1
    f.close()
    return n

def Ratios(elm):
    filename = r'abundance_%s.txt'%elm
    sheetnumb = 0
    n=1
    values = ImportData(filename, sheetnumb, n)
    elements = values[1]
    names = values[2]
    ratio = values[3]
    total = sum(ratio)
    ratio = [i/total for i in ratio]
    return ratio, names, elements


#--------------No Need to Go beyond this point----------------------

def ImportData(filename, sheetnumb, n):
    global content
    global sheet
    values = []

    if filename.endswith('.xlsx') or filename.endswith('.xls'):
        content = xlrd.open_workbook(filename, 'r')

        sheet = content.sheet_by_index(sheetnumb)
        column = sheet.ncols
        row = sheet.nrows
        excel = True

    elif filename.endswith('.csv') or filename.endswith('.txt') or filename.endswith('.dat'):
        content = genfromtxt(filename, dtype=None, skip_header=n)
        
        content = np.atleast_1d(content)
        column = len(content[0])
        row = len(content)
        
        excel = False

    else:
        sys.exit("Wtf, mate?")

    for j in range(column):
        data = []
        counter = 0
        for i in range(0, row):

            try:
                data.append(point(i,j, content, excel))
            except:
                if counter == 0:
                    print('Column %d title:  %s'%(j,point(i,j, excel)))
                    counter +=1
                else:
                    break

        values.append(data)
    return values


def point(i,j, content, excel):
    if excel:
        return sheet.cell(i,j).value
    else:
        return content[i][j]
        
        
if __name__ == '__main__':
   main()
