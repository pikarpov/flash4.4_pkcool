program test
implicit none

type ragged_array
    real*8, allocatable::v(:)
end type ragged_array
type(ragged_array), allocatable::r(:)
integer :: i
character(len=3)::elm

allocate(r(3))
allocate(r(1)%v(2))
allocate(r(2)%v(2))
allocate(r(3)%v(3))

print*, size(r(1)%v(:))
r(1)%v(:) = (/1,3/)
r(2)%v(:) = (/1,2/)
r(3)%v(:) = (/1,2,3/)

do i=1,3
print*, r(i)%v(:) 
enddo
print*, 'yolo'
elm = 'eje'
call execute_command_line(("python cooling_function.py "//elm))
print*, 'done'

end
