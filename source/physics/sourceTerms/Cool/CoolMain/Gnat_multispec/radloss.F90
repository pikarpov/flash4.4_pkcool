!!****if* source/physics/sourceTerms/Cool/CoolMain/Radloss/radloss
!!
!! NAME
!!
!!  radloss
!!
!! SYNOPSIS
!!
!!  radloss(real, intent(IN)  :: t,
!!          real, intent(OUT)  :: radia)
!!
!! DESCRIPTION
!!
!!
!! ARGUMENTS
!!
!!   t :
!!
!!   radia :
!!
!! AUTOGENROBODOC
!!
!!
!!***

!
!       The cooling function is from Sarazin (1986) Rev.Mod.Phys.
!       and from Raymond 1976
!
!       ROUTINE FROM PERES ET AL. 1982, APJ 252, 791

!!****if* source/source_terms/cool/radloss/radloss
!!
!! NAME
!!
!!  radloss
!!
!!
!! SYNOPSIS
!!
!!  call radloss(T, radia)
!!  call radloss(real, real)
!!
!!
!! DESCRIPTION
!!
!!
!!  .
!!
!!
!! ARGUMENTS
!!
!!  T:
!!
!!  radia:
!!
!!***

subroutine radloss(rho, T, elm, radia) !x=rho, y=T
!
  use Cool_data, only : points, Tvs !PK

  implicit none

  integer :: i, locx, locy  !PK
  real*8 :: x, y
  real*8, intent(IN) :: rho, T !x=rho, y=T
  integer, intent(IN) :: elm
  real*8, intent(OUT) :: radia
  !
!
! PK--------------

  !logical :: exists
  !inquire(file="cool.log", exist=exists)
  !if(exists)then
	!open(5,file="cool.log", status="old", position="append", action="write")
  !else
	!open(5,file="cool.log", status="new", action="write")
  !endif
  !write(5,*)"Radloss Active",y
  !close(5)
!  ---------------
  y = T
  do i=1,size(Tvs(elm,:))
    if (Tvs(elm,i)>T) then
        locy = i-1
        exit
    endif
  enddo

  10 format(A,ES10.3)
  radia = points(elm,locy)*(1-(y-Tvs(elm,locy))/(Tvs(elm,locy+1)-Tvs(elm,locy)))+ &
          points(elm,locy+1)*((y-Tvs(elm,locy))/(Tvs(elm,locy+1)-Tvs(elm,locy)))
  return
!----------

end subroutine radloss
