# flash4.4_pkcool

To run the setup: `./setup Sedov_pk_tag -1d +spherical -auto`

The flags can also be found in setup_flags.txt in the head directory

## some key scripts
**Driver_sourceTerms.F90**

Determines when to turn on the cooling based on the amount of mass swept up,
which in turn determines when Sedov profiles are reached. In addition,
the script outputs important shock-front quantities into shock.dat.

**Cool_computeDt.F90**

limits the timesteps to make sure no more than 10% of the total E_int is lost
in a timestep

**Driver_computeDt.F90**

Checks all the *_computeDt.F90 from all the modules

**enuc**

the amount of cooling per cell per timestep; written in each plt and checkpoint file

## source/physics/sourceTerms/Cool/CoolMain/pk_tag/

**Cool.F90**

Calculate the total amount of cooling (radia) and subtract it from the E_int

**Config**

Information about default parameters + the required files

**cooling_table_ext or _int.txt**

Since I have 2 species in my simulation, I have 2 separate cooling functions
1 for internal and 1 for external to the shock material

**radloss.F90**

simple linear interpolation of the cooling function to get the value
at the requested T

**Cool_init.F90**

load up the cooling tables themselves